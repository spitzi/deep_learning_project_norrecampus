import pandas as pd
from flow_data_maker import default_bootstrap_flow_data_maker
from common import EXAMPLE_PLACE_ID, compute_error_statistics, split_timestamp


def _get_detrend_factors(self):
    ser_imp = self._imputation_for_missing_values_and_outliers(self._get_df()).speed_km_hr
    df_train_period = ser_imp[lambda df: df.index < self.split_date]
    return df_train_period \
        .groupby([df_train_period.index.dayofweek, df_train_period.index.time]) \
        .agg(['mean', 'std']) \
        .reset_index() \
        .rename(columns={'level_0': 'day_of_week', 'level_1': 'time_of_day'})

class BaselineHistoricalAverageFlowSingleOutput(object):
    def __init__(self, flow_data_maker):
        self.flow_data_maker = flow_data_maker

    @classmethod
    def name(cls):
        return 'HistoricalAverageOnlyFlow'

    def baseline_errors_statistics(self):
        _, y_train, _, y_test = self.flow_data_maker.get_train_and_test_inputs()
        historical_means = y_train\
            .groupby([y_train.index.dayofweek, y_train.index.time])\
            .mean()\
            .reset_index()\
            .rename(columns={'level_0': 'day_of_week', 'level_1': 'time_of_day'})
        predictions = pd.merge(
            left=historical_means,
            right=split_timestamp(y_test),
            suffixes=['_hist_avg', '_ground_truth'],
            on=['time_of_day', 'day_of_week'],
            how='right') \
            .sort_values(by='index') \
            .f0_hist_avg \
            .values
        errors_df = self.flow_data_maker.individual_errors_without_interpolated_values(predictions)
        return compute_error_statistics(errors_df, 'flow_decile_true', 'flow_decile_predicted')


if __name__ == '__main__':
    print(BaselineHistoricalAverageFlowSingleOutput(default_bootstrap_flow_data_maker(EXAMPLE_PLACE_ID, 12))\
        .baseline_errors_statistics())